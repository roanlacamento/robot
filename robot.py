# A Program that will return the least cost path
# on a grid if the movement is from right and down only

import sys
from datetime import datetime

time_start = datetime.now()
argv = sys.argv
if len(argv) != 2:
    # raise exception if file is missing
    raise Exception("Missing argument. Usage: python robot.py <file_containing_the_input>")

f = open(argv[1])           # open the file
lines = f.readlines()       # read file lines
f.close()                   # close the file

# parse data to create the 'grid'
grid = []
for i, line in enumerate(lines):
    line = line.strip('\n')
    line = line.split()
    temp = [int(i, 16) for i in line]
    grid.append(temp)

grid_size = len(grid)       # detemine the size of the grid

path = []                   # storage of the path

current_cost = grid[0][0]           # will store the current cost
x = 0
y = 0
while x != grid_size:
    while y != grid_size:
        try:
            right = current_cost + grid[x][y+1] # cost on traversing from one point going right
        except IndexError:
            right = float('inf')

        try:
            bottom = current_cost + grid[x+1][y] # cost on traversing from one point going down
        except IndexError:
            bottom = float('inf')

        # resolve the necessary path based on the cost
        if bottom < right:
            current_cost = bottom
            path.append('d')
            break
        else:
            current_cost = right
            y += 1
            if y != grid_size:
                path.append('r')
    x += 1

# Output the path
print ", ".join(path)
time_end = datetime.now()

# print the execution time
time_s = (time_end - time_start).total_seconds()
time_ms = 1000 * time_s
print "executed for %s ms" % time_ms





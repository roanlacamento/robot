Least Cost Path for Robot Problem
===

1. What is the algorithm you used? and why did you decide to use it?

    --- The algorithm used is like a Binary Search Tree. I decided to use this
        because there are only at most two possible choices on going to one node
        on the grid to another node of the grid since we are limited on moving
        down and right only from a specific starting point.

2. Analyze the time and space complexity of the algorithm used.

    --- Time complexity - O(log(n)); Space Complexity - O(1)

3. Write down the least cost path you found, for example: r,d,r,d,r,r,d,r,r,d.

    --- The least I found cost path is d, d, r, r, r, d, d, d, r, r